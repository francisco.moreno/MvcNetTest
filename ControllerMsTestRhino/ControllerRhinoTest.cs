﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;
using SmartIT.Employee.MockDB;
using WebApplication1.Controllers;

namespace ControllerMsTestExamples
{
    [TestClass]
    public class ControllerRhinoTest
    {
         
        TodoController todoController;
        

        [TestInitialize]
        public void setUp()
        {
            todoController = new TodoController();

            todoController._todoRepository.DeleteAll();
            todoController._todoRepository.Add(new SmartIT.Employee.MockDB.Todo { Id = 1, Name = "User 1" });
            todoController._todoRepository.Add(new SmartIT.Employee.MockDB.Todo { Id = 2, Name = "User 2" });
            todoController._todoRepository.Add(new SmartIT.Employee.MockDB.Todo { Id = 3, Name = "User 3" });       
        }

        [TestMethod]
        public void IndexDevuelveTodosLosElementosDelRepositorio()
        {
            
            var res = todoController.Index() as ViewResult;

            var datos = res.Model as List<SmartIT.Employee.MockDB.Todo>;

            int elementosEsperados = 3;

            Assert.AreEqual(elementosEsperados, datos.Count, "El número de datos inicial no es el esperado");

        }

        [TestMethod]
        public void IndexDevuelveTodosLosElementosDelRepositorio_Mock()
        {
            var mock_repository = MockRepository.GenerateMock<ITodoRepository>();

            var mockData = new List<SmartIT.Employee.MockDB.Todo>
                {
                new SmartIT.Employee.MockDB.Todo {Id = 1, Name = "Mock 1" },
                new SmartIT.Employee.MockDB.Todo {Id = 25, Name = "Mock 25"}
            };

            mock_repository.Expect(r => r.GetAll()).Return(mockData);

            todoController._todoRepository = mock_repository;

            var res = todoController.Index() as ViewResult;

            var datos = res.Model as List<SmartIT.Employee.MockDB.Todo>;

            int count = 2;

            Assert.AreEqual(count, datos.Count, "El número de datos inicial no es el esperado");

        }

        [TestMethod]
        public void DeleteTareaQueNoExiste()
        {
            int elementoABorrar = 0;
            int nelementosPrevios = todoController._todoRepository.Count;
            var repoPrevio = todoController._todoRepository.Items;

            var res = todoController.Delete(elementoABorrar, new FormCollection()) as ActionResult;

            int nelementosActuales = todoController._todoRepository.Count;
            var repoActual = todoController._todoRepository.Items;

            Assert.AreEqual(nelementosPrevios, nelementosActuales, "Se ha eliminado un elemento incorrecto");
            CollectionAssert.AreEqual(repoPrevio, repoActual, "Los elementos no coinciden");
        }

        [TestMethod]
        public void DeleteTareaQueNoExisteException_Mock()
        {
            int elementoABorrar = 0;

            var mock_repository = MockRepository.GenerateMock<ITodoRepository>();

            mock_repository.Expect(m => m.FindById(Arg<int>.Is.Equal(elementoABorrar))).Throw(new Exception("Fallo al buscar"));
           


            todoController._todoRepository = mock_repository;

            var res = todoController.Delete(elementoABorrar, new FormCollection()) as ActionResult;


            Assert.AreEqual("Error en el borrado", ((ViewResult)res).ViewName, "El mensaje no coincide");

        }

        [TestMethod]
        public void DeleteTareaQueNoExisteMensajeDevuelto_Mock()
        {
            int elementoABorrar = 0;

            var mock_repository = MockRepository.GenerateMock<ITodoRepository>();

            mock_repository.Expect(m => m.FindById(Arg<int>.Is.Equal(elementoABorrar))).Return(null);


            todoController._todoRepository = mock_repository;

            var res = todoController.Delete(elementoABorrar, new FormCollection()) as ActionResult;


            Assert.AreEqual("Elemento no existe", ((ViewResult)res).ViewName, "El mensaje devuelto no es correcto");

        }

        [TestMethod]
        public void DeleteTareaExceptionBorrado_Mock()
        {
            int elementoABorrar = 0;

            var mock_repository = MockRepository.GenerateMock<ITodoRepository>();

            mock_repository.Expect(m => m.FindById(Arg<int>.Is.Equal(elementoABorrar))).Return(new SmartIT.Employee.MockDB.Todo { Id = 1, Name = "Mock 1" });
            mock_repository.Expect(m => m.Delete(Arg<SmartIT.Employee.MockDB.Todo>.Is.Anything)).Throw(new Exception("Fallo al borrar"));


            todoController._todoRepository = mock_repository;

            var res = todoController.Delete(elementoABorrar, new FormCollection()) as ActionResult;


            Assert.AreEqual("Error en el borrado", ((ViewResult)res).ViewName, "El mensaje no coincide");

        }

        [TestMethod]
        public void DeleteTareaQueExistente()
        {
            int elementoABorrar = 1;
            int nelementosPrevios = todoController._todoRepository.Count;

            var res = todoController.Delete(elementoABorrar, new FormCollection()) as ActionResult;

            int nelementosActuales = todoController._todoRepository.Count;

            Assert.AreEqual(nelementosPrevios - 1, nelementosActuales, "Se ha eliminado un elemento incorrecto");
        }
    }
}
