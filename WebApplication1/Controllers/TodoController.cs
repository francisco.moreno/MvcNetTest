﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SmartIT.Employee.MockDB;

namespace WebApplication1.Controllers
{
    public class TodoController : Controller
    {
        public ITodoRepository _todoRepository;

        public TodoController()
        {
            _todoRepository = new TodoRepository();

        }
        // GET: Todo
        public ActionResult Index()
        {
            return View((List < SmartIT.Employee.MockDB.Todo >)_todoRepository.GetAll());
        }

        // GET: Todo/Details/5
        public ActionResult Details(int id)
        {
            var findTodo = _todoRepository.FindById(id);
            return View(findTodo);
        }

        // GET: Todo/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Todo/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                _todoRepository.Add(new SmartIT.Employee.MockDB.Todo() { Name = collection["Name"]});

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Todo/Edit/5
        public ActionResult Edit(int id)
        {
            var findTodo = _todoRepository.FindById(id);
            return View(findTodo);
        }

        // POST: Todo/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                var findTodo = _todoRepository.FindById(id);
                findTodo.Name = collection["Name"];

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Todo/Delete/5
        public ActionResult Delete(int id)
        {
            return View(_todoRepository.FindById(id));
        }

        // POST: Todo/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                var findTodo = _todoRepository.FindById(id);
                if (findTodo == null)
                    return View("Elemento no existe");
                _todoRepository.Delete(findTodo);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View("Error en el borrado");
            }
        }
    }
}
